<?php

namespace App\Controller;

use App\Entity\Topic;
use App\Form\TopicFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class TopicController extends AbstractController
{
    /**
     * @Route("/topic/new", name="new_topic")
     * 
     * @return void
     */
    public function new(Request $request, EntityManagerInterface $em)
    {

        $topic = new Topic();
        $form = $this->createForm(TopicFormType::class, $topic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $topic->setUser($this->getUser());
            $em->persist($topic);
            $em->flush();
            return $this->redirect("/");
        }
        return $this->render('new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
