<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="landing")
     */
    public function index()
    {
        if ($this->getUser() != null) {

            return $this->render('main.html.twig');
        }

        return $this->render('home.html.twig');
    }
}
